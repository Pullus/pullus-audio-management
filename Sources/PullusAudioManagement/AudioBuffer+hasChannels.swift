import CoreAudio

public
extension AudioBuffer {
	
	///
	/// True if this audio buffer has at least one channel.
	///
	public
	var hasChannels: Bool { return mNumberChannels > 0 }
	
}
