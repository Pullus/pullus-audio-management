import CoreAudio

public
extension AudioObjectID {
	
	private typealias SELF = AudioObjectID
	
	private static let deviceForUIDProperty = AudioObjectPropertyAddress(
		mSelector: kAudioHardwarePropertyDeviceForUID,
		mScope: kAudioObjectPropertyScopeGlobal,
		mElement: kAudioObjectPropertyElementMaster
	)
	
	func lookup(by uid: String) -> AudioDeviceID? {
		var cfUID = (uid as CFString)
		var deviceID = kAudioObjectUnknown
		var translation = AudioValueTranslation(
			mInputData: &cfUID,
			mInputDataSize: UInt32(MemoryLayout<CFString>.size),
			mOutputData: &deviceID,
			mOutputDataSize: UInt32(MemoryLayout<AudioObjectID>.size)
		)
				
		var size = UInt32(MemoryLayout<AudioValueTranslation>.size)
		var property = SELF.deviceForUIDProperty
		
		let status = AudioObjectGetPropertyData(self, &property, UInt32(0), nil, &size, &translation)

		guard status == noErr && deviceID != kAudioObjectUnknown else { return nil }
		
		return deviceID
	}
	
}
