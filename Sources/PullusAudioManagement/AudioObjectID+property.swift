import os.log
import CoreAudio

public
extension AudioObjectID {
	
	///
	/// Checks if the audio object that is associated with this audio object ID
	/// has the specified property.
	///
	public
	func has(property: AudioObjectPropertyAddress) -> Bool {
		var address = property
		
		return AudioObjectHasProperty(self, &address)
	}
	
	///
	/// Checks if the audio object that is associated with this audio object ID
	/// has the specified property and that this property is writable.
	///
	public
	func isSettable(property: AudioObjectPropertyAddress) -> Bool {
		guard has(property: property) else {
			os_log("Can't check the writeability of property of audio object 0x%04x. Property is missing.", type: .error, self)
			return false
		}
		
		var address = property
		var settable = DarwinBoolean(true)
		
		let status = AudioObjectIsPropertySettable(self, &address, &settable)
		
		guard status == noErr else {
			os_log("Can't check the writeability of property of audio object 0x%04x.", type: .error, self)
			return false
		}
		
		return settable.boolValue
	}
	
	///
	/// Returns the size of a specified property of the audio object that is
	/// associated with this audio object ID.
	///
	/// If the audio object has not the specified property or the property is
	/// unreadable the result is `nil`.
	///
	public
	func size(property: AudioObjectPropertyAddress) -> UInt32? {
		guard has(property: property) else {
			os_log("Can't get the size of property of audio object 0x%04x. Property is missing.", type: .error, self)
			return nil
		}
		
		var address = property
		var size: UInt32 = 0
		
		let status = AudioObjectGetPropertyDataSize(self, &address, 0, nil, &size)
		
		guard status == noErr else {
			os_log("Can't get the size of property of audio object 0x%04x.", type: .error, self)
			return nil
		}
		
		return size
	}
	
	///
	/// Sets the value of a specified property of the audio object that is
	/// associated with this audio object ID.
	///
	/// If the audio object has not the specified property or the property is
	/// unreadable the result is `false`.
	///
	@discardableResult
	public
	func set <VALUE : Initializable> (property: AudioObjectPropertyAddress, value: VALUE) -> Bool {
		guard isSettable(property: property) else {
			os_log("Can't set property of audio object 0x%04x. Property is not writable.", type: .error, self)
			return false
		}
		
		var address = property
		let size = UInt32(MemoryLayout<VALUE>.size)
		var value = value
		
		let status = AudioObjectSetPropertyData(self, &address, 0, nil, size, &value)
		
		guard status == noErr else {
			os_log("Can't set property of audio object 0x%04x.", type: .error, self)
			return false
		}
		
		return true
	}
	
	///
	/// Returns the value of a specified property of the audio object that is
	/// associated with this audio object ID.
	///
	/// If the audio object has not the specified property or the property is
	/// unreadable the result is `nil`.
	///
	public
	func get <VALUE : Initializable> (property: AudioObjectPropertyAddress) -> VALUE? {
		return get(property: property, template: VALUE())
	}
	
	///
	/// Returns the value of a specified (`String`) property of the audio object
	/// that is associated with this audio object ID.
	///
	/// If the audio object has not the specified property or the property is
	/// unreadable the result is `nil`.
	///
	/// This special `get` is necessary because `CFString` is not and can not be
	/// `Initializable`.
	///
	public
	func get(property: AudioObjectPropertyAddress) -> String? {
		return get(property: property, template: "" as CFString) as String?
	}
	
	///
	/// Returns the value of a specified (`Bool`) property of the audio object
	/// that is associated with this audio object ID.
	///
	/// If the audio object has not the specified property or the property is
	/// unreadable the result is `nil`.
	///
	/// This special `get` is necessary because `Bool` .
	///
	public
	func get(property: AudioObjectPropertyAddress) -> Bool? {
		guard has(property: property) else {
			os_log("Can't get property of audio object 0x%04x. Property is missing.", type: .error, self)
			return nil
		}
		
		var address = property
		var size = UInt32(4)
		var value = false
		
		let status = AudioObjectGetPropertyData(self, &address, 0, nil, &size, &value)
		
		guard status == noErr else {
			os_log("Can't get property of audio object 0x%04x.", type: .error, self)
			return nil
		}

		return value
	}
	
	///
	/// Returns the value of a specified (`Array`) property of the audio object
	/// that is associated with this audio object ID.
	///
	/// If the audio object has not the specified property or the property is
	/// unreadable the result is `nil`.
	///
	public
	func get <VALUE : Initializable> (property: AudioObjectPropertyAddress) -> [VALUE]? {
		guard var size: UInt32 = size(property: property) else {
			os_log("Can't get property of audio object 0x%04x.", type: .error, self)
			return nil
		}
		
		var address = property
		let count = Int(size) / MemoryLayout<VALUE>.size
		var values: [VALUE] = Array(repeating: VALUE(), count: count)

		let status = AudioObjectGetPropertyData(self, &address, 0, nil, &size, &values)
		
		guard status == noErr else {
			os_log("Can't get property of audio object 0x%04x.", type: .error, self)
			return nil
		}
		
		return values
	}
	
	///
	/// - Note: Doesn't work with arrays.
	///
	private
	func get <VALUE> (property: AudioObjectPropertyAddress, template: VALUE) -> VALUE? {
		guard has(property: property) else {
			os_log("Can't get property of audio object 0x%04x. Property is missing.", type: .error, self)
			return nil
		}
		
		var address = property
		var size = UInt32(MemoryLayout<VALUE>.size)
		var value = template
		
		let status = AudioObjectGetPropertyData(self, &address, 0, nil, &size, &value)
		
		guard status == noErr else {
			os_log("Can't get property of audio object 0x%04x.", type: .error, self)
			return nil
		}
		
		return value
	}
	
}
