import CoreAudio

public
extension AudioDeviceID {
	
	public
	var isInputDevice: Bool { return inputStreamConfiguration?.contains { $0.hasChannels } ?? false }
	
	public
	var isOutputDevice: Bool { return outputStreamConfiguration?.contains { $0.hasChannels } ?? false }

}
