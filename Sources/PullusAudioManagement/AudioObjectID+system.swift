import CoreAudio

public
extension AudioObjectID {
	
	///
	/// Returns "… The AudioObjectID that always refers to the one and only
	/// instance of the AudioSystemObject class. …"
	///
	public
	static var system: AudioObjectID { return AudioObjectID(kAudioObjectSystemObject) }
	
}
