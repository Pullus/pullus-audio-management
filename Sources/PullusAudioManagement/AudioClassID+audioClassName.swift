import CoreAudio

public
extension AudioClassID {
	
	///
	/// The name of the "audio class" that is associated with this audio class
	/// ID.
	///
	public
	var audioClassName: String? {
		switch self {
		case kAudioDeviceClassID:			return "AudioDevice"
		case kAudioSubDeviceClassID:		return "AudioSubDevice"
		case kAudioAggregateDeviceClassID:	return "AudioAggregateDevice"
		case kAudioEndPointClassID:			return "AudioEndPoint"
		case kAudioEndPointDeviceClassID:	return "AudioEndPointDevice"
		default:							return nil
		}
	}
	
}
