import CoreAudio

///
/// Annotate that `AudioObjectID` has an `init()`.
///
/// Necessary for `AudioObjectID.get(property:)`.
///
extension AudioObjectID : Initializable {
}
