import Foundation
import CoreAudio

extension OSStatus {
	
	///
	/// The name of an `OSStatus` as far as known.
	///
	/// 
	///
	var osStatusName: String? {
		switch self {
		case noErr:										return "No error"
		case kAudioHardwareNoError:						return "No audio hardware error"

		case kAudioDevicePermissionsError:				return "Device permissions"
		case kAudioDeviceUnsupportedFormatError:		return "Device unsupported format"
		case kAudioDeviceUnsupportedFormatError:		return "Unsupported format"
		case kAudioHardwareBadDeviceError:				return "Hardware bad device"
		case kAudioHardwareBadObjectError:				return "Hardware bad object"
		case kAudioHardwareBadPropertySizeError:		return "Hardware bad property size"
		case kAudioHardwareBadStreamError:				return "Hardware bad stream"
		case kAudioHardwareIllegalOperationError:		return "Hardware illegal operation"
		case kAudioHardwareNotRunningError:				return "Hardware not running"
		case kAudioHardwareUnknownPropertyError:		return "Hardware unknown property"
		case kAudioHardwareUnspecifiedError:			return "Hardware unspecified"
		case kAudioHardwareUnsupportedOperationError:	return "Hardware unsupported operation"

		default:										return nil
		}
	}
	
}
