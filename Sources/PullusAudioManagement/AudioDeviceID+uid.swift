import CoreAudio

extension AudioDeviceID {
	
	private typealias SELF = AudioDeviceID

	///
	/// The address to access the "name" property of an audio device.
	///
	public
	static let uidProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyDeviceUID,
		mScope: kAudioObjectPropertyScopeGlobal,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// The "name" property of the audio device associated with this audio
	/// device ID.
	///
	public
	var uid: String? {
		return get(property: SELF.uidProperty)
	}
	
}
