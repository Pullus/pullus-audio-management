import CoreAudio

public
class AudioManager {
	
	private typealias SELF = AudioManager
	
	private
	init() {
	}

	// MARK: - Default Output Device
	
	///
	/// The audio device ID of the default output device.
	///
	private
	static var defaultOutputDevice: AudioDeviceID? { return AudioObjectID.system.defaultOutputDevice }
	
	///
	/// The volume of the default output device.
	///
	public
	static var defaultOutputVolume: Float {
		get {
			return SELF.defaultOutputDevice?.outputVolume ?? 0
		}
		set {
			var device = SELF.defaultOutputDevice
			
			device?.outputVolume = newValue
		}
	}
	
	///
	/// The mute state of the default output device.
	///
	public
	static var defaultOutputMute: Bool {
		get {
			return SELF.defaultOutputDevice?.outputMute ?? false
		}
		set {
			var device = SELF.defaultOutputDevice
			
			device?.outputMute = newValue
		}
	}
	
	// MARK: - Default Input Device

	///
	/// The audio device ID of the default input device.
	///
	private
	static var defaultInputDevice: AudioDeviceID? { return AudioObjectID.system.defaultInputDevice }
	
	///
	/// The volume of the default input device.
	///
	public
	static var defaultInputVolume: Float {
		get {
			return SELF.defaultInputDevice?.outputVolume ?? 0
		}
		set {
			var device = SELF.defaultInputDevice
			
			device?.outputVolume = newValue
		}
	}
	
	///
	/// The mute state of the default input device.
	///
	public
	static var defaultInputMute: Bool {
		get {
			return SELF.defaultInputDevice?.outputMute ?? false
		}
		set {
			var device = SELF.defaultInputDevice
			
			device?.outputMute = newValue
		}
	}

}
