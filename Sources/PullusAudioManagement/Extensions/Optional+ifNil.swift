import Swift

extension Optional {
	
	///
	/// Calls a closure if this optional is `nil` (`.none`) and returns `self`.
	///
	@discardableResult
	func ifNil(_ block: () throws -> Void) rethrows -> Optional {
		if self == nil { try block() }
		
		return self
	}
	
}
