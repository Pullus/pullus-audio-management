///
/// Marker that a type has an `init()`.
///
public
protocol Initializable {
	
	init()
	
}
