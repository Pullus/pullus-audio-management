import Swift

extension Bool {
	
	///
	/// Calls a closure if this boolean is `true` and returns `self`.
	///
	@discardableResult
	func ifFalse(_ block: () throws -> Void) rethrows -> Bool {
		if !self { try block() }
		
		return self
	}
	
}
