import os.log
import AudioToolbox

public
extension AudioDeviceID {
	
	private typealias SELF = AudioDeviceID

	// MARK: - Input Device
	
	///
	/// The address to access the "input volume" property of an audio device.
	///
	public
	static let inputVolumeProperty = AudioObjectPropertyAddress(
		mSelector: kAudioHardwareServiceDeviceProperty_VirtualMasterVolume,
		mScope: kAudioDevicePropertyScopeInput,
		mElement: kAudioObjectPropertyElementMaster)
	
	public
	var hasInputVolume: Bool {
		return has(property: SELF.inputVolumeProperty)
	}
	
	///
	/// The "input volume" property of the audio device associated with this
	/// audio device ID.
	///
	/// Returns `0.0` if the volume property can't be read.
	///
	public
	var inputVolume: Float? {
		get { return getVolume(property: SELF.inputVolumeProperty) }
		set { newValue.map { setVolume(property: SELF.inputVolumeProperty, $0) } }
	}
	
	// MARK: - Output Device

	///
	/// The address to access the "output volume" property of an audio device.
	///
	public
	static let outputVolumeProperty = AudioObjectPropertyAddress(
		mSelector: kAudioHardwareServiceDeviceProperty_VirtualMasterVolume,
		mScope: kAudioDevicePropertyScopeOutput,
		mElement: kAudioObjectPropertyElementMaster)
	
	public
	var hasOutputVolume: Bool {
		return has(property: SELF.outputVolumeProperty)
	}
	///
	/// The "output volume" property of the audio device associated with this
	/// audio device ID.
	///
	/// Returns `0.0` if the volume property can't be read.
	///
	public
	var outputVolume: Float? {
		get { return getVolume(property: SELF.outputVolumeProperty) }
		set { newValue.map { setVolume(property: SELF.outputVolumeProperty, $0) } }
	}
	
	// MARK: -

	///
	/// Returns the "volume" property of the audio device associated with this
	/// audio device ID.
	///
	/// If this device has no volume property the result is `nil`.
	///
	private
	func getVolume(property: AudioObjectPropertyAddress) -> Float? {
		let value: Float32? = get(property: property)
		
		return value
			.ifNil { os_log("Can't get volume of audio device 0x%04x.", type: .error, self) }
			.map { $0.atLeast(0.0).atMost(1.0) }
	}
	
	///
	/// Sets the "volume" property of the audio device associated with this
	/// audio device ID.
	///
	/// If this device has no volume property or the property is not writable
	/// the call is quietly discarded.
	///
	private
	func setVolume(property: AudioObjectPropertyAddress, _ aLevel: Float) {
		let value = aLevel.atLeast(0.0).atMost(1.0)
		
		set(property: property, value: value)
			.ifFalse { os_log("Can't set volume of audio device 0x%04x.", type: .error, self) }
	}
	
}
