import os.log
import CoreAudio

extension AudioDeviceID {
	
	private typealias SELF = AudioDeviceID
	
	// MARK: - Input Device
	
	public
	static let inputStreamConfigurationProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyStreamConfiguration,
		mScope: kAudioDevicePropertyScopeInput,
		mElement: 0)
	
	public
	var inputStreamConfiguration: [AudioBuffer]? {
		return streamConfiguration(property: SELF.inputStreamConfigurationProperty)
	}
	
	// MARK: - Output Device
	
	public
	static let outputStreamConfigurationProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyStreamConfiguration,
		mScope: kAudioDevicePropertyScopeOutput,
		mElement: 0)
	
	public
	var outputStreamConfiguration: [AudioBuffer]? {
		return streamConfiguration(property: SELF.outputStreamConfigurationProperty)
	}
	
	// MARK: -
	
	private
	func streamConfiguration(property: AudioObjectPropertyAddress) -> [AudioBuffer]? {
		guard var bufferCount: UInt32 = size(property: property) else {
			os_log("Can't get audio buffers of audio object 0x%04x.", type: .error, self)
			return nil
		}
		
		var address = property
		let buffers = AudioBufferList.allocate(maximumBuffers: Int(bufferCount))
		defer { free(buffers.unsafeMutablePointer) }
		
		let status = AudioObjectGetPropertyData(self, &address, 0, nil, &bufferCount, buffers.unsafeMutablePointer)
		
		guard status == noErr else {
			os_log("Can't get audio buffers of audio object 0x%04x.", type: .error, self)
			return nil
		}
		
		return buffers.map { $0 }
	}
	
}
