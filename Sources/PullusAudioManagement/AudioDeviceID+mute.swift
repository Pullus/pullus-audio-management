import os.log
import CoreAudio

public
extension AudioDeviceID {
	
	private typealias SELF = AudioDeviceID

	// MARK: - Input Device
	
	///
	/// The address to access the "input mute" property of an audio input
	/// device.
	///
	public
	static let inputMuteProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyMute,
		mScope: kAudioDevicePropertyScopeInput,
		mElement: kAudioObjectPropertyElementMaster)
	
	public
	var hasInputMute: Bool {
		return has(property: SELF.inputMuteProperty)
	}
	
	///
	/// The "input mute" property of the audio device associated with this
	/// audio device ID.
	///
	/// Returns `false` if the mute property can't be read.
	///
	public
	var inputMute: Bool? {
		get { return getMute(property: SELF.inputMuteProperty) }
		set { newValue.map { setMute(property: SELF.inputMuteProperty, $0) } }
	}
	
	// MARK: - Output Device

	///
	/// The address to access the "output mute" property of an audio output
	/// device.
	///
	public
	static let outputMuteProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyMute,
		mScope: kAudioDevicePropertyScopeOutput,
		mElement: kAudioObjectPropertyElementMaster)
	
	public
	var hasOutputMute: Bool {
		return has(property: SELF.outputMuteProperty)
	}
	
	///
	/// The "output mute" property of the audio device associated with this
	/// audio device ID.
	///
	/// Returns `false` if the mute property can't be read.
	///
	public
	var outputMute: Bool? {
		get { return getMute(property: SELF.outputMuteProperty) }
		set { newValue.map { setMute(property: SELF.outputMuteProperty, $0) } }
	}
	
	// MARK: -
	
	///
	/// Returns the "mute" property of the audio device associated with this
	/// audio device ID.
	///
	/// If this device has no mute property the result is `nil`.
	///
	private
	func getMute(property: AudioObjectPropertyAddress) -> Bool? {
		let value: UInt32? = get(property: property)
		
		return value
			.ifNil { os_log("Can't get mute/unmute of audio device 0x%04x.", type: .error, self) }
			.map { $0 == 1 }
	}
	
	///
	/// Sets the "mute" property of the audio device associated with this audio
	/// device ID.
	///
	/// If this device has no mute property or the property is not writable the
	/// call is quietly discarded.
	///
	private
	func setMute(property: AudioObjectPropertyAddress, _ isMuted: Bool) {
		let value: UInt = isMuted ? 1 : 0

		set(property: property, value: value)
			.ifFalse { os_log("Can't mute/unmute audio device 0x%04x.", type: .error, self) }
	}
	
}
