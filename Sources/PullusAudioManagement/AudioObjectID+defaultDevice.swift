import os.log
import CoreAudio

public
extension AudioObjectID {
	
	private typealias SELF = AudioObjectID

	// MARK: - Input Device
	
	///
	/// The address to access the "default input device" property of an audio
	/// object.
	///
	public
	static let defaultInputDeviceProperty = AudioObjectPropertyAddress(
		mSelector: kAudioHardwarePropertyDefaultInputDevice,
		mScope: kAudioObjectPropertyScopeGlobal,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// Returns the "default input device" of the audio object associated with
	/// this audio object ID.
	///
	/// Should be applied to "AudioSystemObject".
	///
	public
	var defaultInputDevice: AudioDeviceID? {
		return defaultDevice(property: SELF.defaultInputDeviceProperty)
	}
	
	// MARK: - Output Device
	
	///
	/// The address to access the "default output device" property of an audio
	/// object.
	///
	public
	static let defaultOutputDeviceProperty = AudioObjectPropertyAddress(
		mSelector: kAudioHardwarePropertyDefaultOutputDevice,
		mScope: kAudioObjectPropertyScopeGlobal,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// Returns the "default output device" of the audio object associated with
	/// this audio object ID.
	///
	/// Should be applied to "AudioSystemObject".
	///
	public
	var defaultOutputDevice: AudioDeviceID? {
		return defaultDevice(property: SELF.defaultOutputDeviceProperty)
	}
	
	// MARK: -
	
	///
	/// Returns a "default device" of the audio object associated with this
	/// audio object ID.
	///
	/// Should be applied to "AudioSystemObject".
	///
	private
	func defaultDevice(property: AudioObjectPropertyAddress) -> AudioDeviceID? {
		guard
			let result: AudioObjectID = get(property: property),
			result != kAudioObjectUnknown
			else {
				os_log("Can't get default device of audio object 0x%04x.", type: .error, self)
				return nil
		}
		
		return result
	}
	
}
