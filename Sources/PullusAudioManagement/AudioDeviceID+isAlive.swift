import CoreAudio

extension AudioDeviceID {
	
	private typealias SELF = AudioObjectID
	
	///
	/// True if the input or output of the device that is associated with this
	/// `AudioDeviceID` is alive.
	///
	/// Is `false` if the the device is unknown.
	///
	public
	var isAlive: Bool {
		return isInputAlive || isOutputAlive
	}
	
	///
	/// The address to access the "isAlive" property of an audio input device.
	///
	public
	static let isInputAliveProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyDeviceIsAlive,
		mScope: kAudioDevicePropertyScopeInput,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// True if the input of the device that is associated with this
	/// `AudioDeviceID` is alive.
	///
	public
	var isInputAlive: Bool {
		return get(property: SELF.isInputAliveProperty) ?? false
	}
	
	///
	/// The address to access the "isAlive" property of an audio output device.
	///
	public
	static let isOutputAliveProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyDeviceIsAlive,
		mScope: kAudioDevicePropertyScopeOutput,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// True if the output of the device that is associated with this
	/// `AudioDeviceID` is alive.
	///
	/// Is `false` if the the device is unknown.
	///
	public
	var isOutputAlive: Bool {
		return get(property: SELF.isOutputAliveProperty) ?? false
	}
	
}
