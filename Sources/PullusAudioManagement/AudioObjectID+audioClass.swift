import os.log
import CoreAudio

public
extension AudioObjectID {
	
	private typealias SELF = AudioObjectID

	///
	/// The address to access the "class" property of an audio object.
	///
	public
	static let audioClassProperty = AudioObjectPropertyAddress(
		mSelector: kAudioObjectPropertyClass,
		mScope: kAudioObjectPropertyScopeGlobal,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// The "class" property of the audio object associated with this audio
	/// object ID.
	///
	public
	var audioClass: AudioClassID? {
		return get(property: SELF.audioClassProperty)
			.ifNil { os_log("Can't get class of audio object 0x%04x.", type: .error, self) }
	}
	
}
