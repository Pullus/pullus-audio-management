import CoreAudio

extension AudioDeviceID {
	
	private typealias SELF = AudioObjectID

	///
	/// The address to access the "name" property of an audio device.
	///
	public
	static let nameProperty = AudioObjectPropertyAddress(
		mSelector: kAudioDevicePropertyDeviceNameCFString,
		mScope: kAudioObjectPropertyScopeGlobal,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// The "name" property of the audio device associated with this audio
	/// device ID.
	///
	public
	var name: String? {
		return get(property: SELF.nameProperty)
	}
	
}
