import os.log
import CoreAudio

public
extension AudioObjectID {
	
	private typealias SELF = AudioObjectID

	///
	/// The address to access the "all devices" property of an audio object.
	///
	public
	static let devicesProperty = AudioObjectPropertyAddress(
		mSelector: kAudioHardwarePropertyDevices,
		mScope: kAudioObjectPropertyScopeGlobal,
		mElement: kAudioObjectPropertyElementMaster)
	
	///
	/// The "all devices" property of the audio object associated with this
	/// audio object ID.
	///
	/// Should be applied to "AudioSystemObject".
	///
	public
	var devices: [AudioDeviceID] {
		return get(property: SELF.devicesProperty)
			.ifNil { os_log("Can't get devices of audio object 0x%04x.", type: .error, self) }
			?? []
	}
	
}
