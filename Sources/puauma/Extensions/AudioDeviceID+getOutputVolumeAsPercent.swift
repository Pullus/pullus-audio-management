import CoreAudio

import PullusAudioManagement

extension AudioDeviceID {
	
	func getOutputVolumeAsPercent() -> Int? {
		return outputVolume.map { Int($0 * 100) }
	}
	
}
