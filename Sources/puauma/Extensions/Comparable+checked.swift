import Swift

extension Comparable {
	
	///
	/// Checks if this float is contained in an interval and returns this
	/// value.
	///
	/// If this value is outside the interval, an exception is raised with the
	/// specified message.
	///
	func checked(in range: ClosedRange<Self>, onErrorThrow message: String) throws -> Self {
		guard range.contains(self) else { throw Failure(message) }
		
		return self
	}
	
}
