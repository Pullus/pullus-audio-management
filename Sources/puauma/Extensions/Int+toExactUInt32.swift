import Swift

extension Int {
	
	func toExactUInt32() -> UInt32? {
		return UInt32(exactly: self)
	}
	
}
