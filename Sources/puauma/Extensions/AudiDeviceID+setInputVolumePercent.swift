import CoreAudio

import PullusAudioManagement

extension AudioDeviceID {

	func setInputVolume(percent: Int) {
		var copy = self

		copy.inputVolume = Float(percent) / 100.0
	}
	
}
