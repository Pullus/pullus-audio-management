import Swift

extension String {
	
	///
	/// Convenience function for chaining.
	///
	func toFloat() -> Float? {
		return Float(self)
	}
	
}
