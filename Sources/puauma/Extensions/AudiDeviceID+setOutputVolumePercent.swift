import CoreAudio

import PullusAudioManagement

extension AudioDeviceID {

	func setOutputVolume(percent: Int) {
		var copy = self

		copy.outputVolume = Float(percent) / 100.0
	}
	
}
