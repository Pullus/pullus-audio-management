import Swift

extension Bool {
	
	func toYesNo() -> String {
		return self ? "yes" : "no"
	}
	
}
