import CoreAudio

import PullusAudioManagement

extension AudioDeviceID {
	
	func getInputVolumeAsPercent() -> Int? {
		return inputVolume.map { Int($0 * 100) }
	}
	
}
