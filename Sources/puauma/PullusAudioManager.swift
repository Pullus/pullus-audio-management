import Foundation
import CoreAudio

import PullusAudioManagement

enum Option {
	
	case set(level: Int)
	case change(delta: Int)
	case mute
	case unmute
	
	func apply(output device: AudioDeviceID) {
		var device = device
		
		switch self {
		case .mute:
			device.outputMute = true
			
		case .unmute:
			device.outputMute = false
			
		case let .set(level):
			device.setOutputVolume(percent: level)
			
		case let .change(delta):
			if let volume = device.getOutputVolumeAsPercent() {
				device.setOutputVolume(percent: volume + delta )
			}
		}
	}
	
	func apply(input device: AudioDeviceID) {
		var device = device
		
		switch self {
		case .mute:
			device.inputMute = true
			
		case .unmute:
			device.inputMute = false
			
		case let .set(level):
			device.setInputVolume(percent: level)
			
		case let .change(delta):
			if let volume = device.getOutputVolumeAsPercent() {
				device.setInputVolume(percent: volume + delta )
			}
		}
	}
	
}

class PullusAudioManager {
	
	///
	/// The input arguments used as input queue.
	///
	private
	var arguments: ArraySlice<String> = []
	
	///
	/// Tries to read the first argument from `arguments` and tries to "convert"
	/// it to a string.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	private
	func parseString(onErrorThrow message: String) throws -> String {
		guard let value = arguments.popFirst() else { throw Failure(message) }
		
		return value
	}
	
	///
	/// Tries to read the first argument from `arguments` and tries to convert
	/// it to a float.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	private
	func parseFloat(onErrorThrow message: String) throws -> Float {
		guard
			let value = arguments.popFirst()?.toFloat()
			else { throw Failure(message) }
		
		return value
	}
	
	///
	/// Tries to read the first argument from `arguments` and tries to convert
	/// it to an integer.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	private
	func parseInt(onErrorThrow message: String) throws -> Int {
		guard
			let value = arguments.popFirst()?.toInt()
			else { throw Failure(message) }
		
		return value
	}
	
	// MARK: -
	
	///
	/// Starts the parsing of the arguments and executes the commands.
	///
	func run(arguments anArguments: [String]) -> Bool {
		do {
			// Drop the name of the executable (puauma)
			arguments = anArguments.dropFirst()
			
			try parseCommand()
			
			guard
				arguments.isEmpty
				else { throw Failure("ignored supernumerary arguments") }
			
			return true
		} catch {
			printUsage(String(describing: error))
			return false
		}
	}
	
	//swiftlint:disable function_body_length
	private
	func printUsage(_ message: String? = nil) {
		print("NAME")
		print("    puauma - Pullus Audio Manager")
		print()
		print("SYNOPSIS")
		print("    Manage connected audio devices.")
		print()
		print("    help")
		print("        Shows this text.")
		print()
		print("    list")
		print("        Shows all audio device.")
		print()
		print("    show")
		print("        Shows the volume and the mute/unmute state of the default audio device.")
		print()
		print("    volume set <level>")
		print("        Sets the volume of the default audio device to a level (0 to 100).")
		print()
		print("    volume change <delta>")
		print("        Changes the volume of the default audio device by a delta (-100 to 100).")
		print()
		print("    volume mute")
		print("        Mutes the default audio device.")
		print()
		print("    volume unmute")
		print("        Unmutes the default audio device.")
		print()
		print("    sensitivity set <level>")
		print("        Sets the sensitivity of the default audio input device to a level (0 to 100).")
		print()
		print("    sensitivity change <delta>")
		print("        Changes the sensitivity of the default audio device by a delta (-100 to 100).")
		print()
		print("    sensitivity mute")
		print("        Mutes the default audio device.")
		print()
		print("    sensitivity unmute")
		print("        Unmutes the default audio device.")
		print()
		print("    output <device> set <level>")
		print("        Sets the volume of the specifided audio output device to a level (0 to 100).")
		print()
		print("    output <device> change <delta>")
		print("        Changes the volume of the specifided audio output device by a delta (-100 to 100).")
		print()
		print("    output <device> mute")
		print("        Mutes the specifided audio output device.")
		print()
		print("    output <device> unmute")
		print("        Unmutes the specifided audio output device.")
		print()
		print("    input <device> set <level>")
		print("        Sets the volume of the specifided audio input device to a level (0 to 100).")
		print()
		print("    input <device> change <delta>")
		print("        Changes the volume of the specifided audio input device by a delta (-100 to 100).")
		print()
		print("    input <device> mute")
		print("        Mutes the specifided audio input device.")
		print()
		print("    input <device> unmute")
		print("        Unmutes the specifided audio input device.")
		print()
		print()
		print("    *) A <device> is an ID or an UID. Both are shown with `list`.")
		print()
		print("EXAMPLES")
		print("    puauma volume mute")
		print()
		print("    Mutes the default audio device.")
		print()
		print("AUTHOR")
		print("    Frank Schuster")
		
		if let message = message {
			print()
			print("Failure:", message)
		}
	}
	//swiftlint:enable function_body_length
	
	///
	/// ```
	/// <command> ::= "help"
	///             | "list"
	///             | "show"
	///             | "volume" "set" <level>
	///             | "volume" "change" <delta>
	///             | "volume" "mute"
	///             | "volume" "unmute"
	///             | "sensitivity" "set" <level>
	///             | "sensitivity" "change" <delta>
	///             | "sensitivity" "mute"
	///             | "sensitivity" "unmute"
	///             | "output" <device> "set" <level>
	///             | "output" <device> "change" <delta>
	///             | "output" <device> "mute"
	///             | "output" <device> "unmute"
	///             | "input" <device> "set" <level>
	///             | "input" <device> "change" <delta>
	///             | "input" <device> "mute"
	///             | "input" <device> "unmute"
	///             .
	/// ```
	private
	func parseCommand() throws {
		switch try parseString(onErrorThrow: "Command is missing.") {
		case "help":			printUsage()
		case "list":			try parseList()
		case "show":			try parseShow()
		case "volume":			try parseLevelOption().apply(output: defaultOutputDevice())
		case "sensitivity":		try parseLevelOption().apply(input: defaultInputDevice())
		
		case "output":
			let device = try parseDevice()
			try parseLevelOption().apply(output: device)

		case "input":
			let device = try parseDevice()
			try parseLevelOption().apply(input: device)
			
		default:				throw Failure("Unknown command")
		}
	}
	
	private
	func defaultOutputDevice() throws -> AudioDeviceID {
		guard let device = AudioObjectID.system.defaultOutputDevice else {
			throw Failure("Can't retrieve default output device.") }
		
		return device
	}
	
	private
	func defaultInputDevice() throws -> AudioDeviceID {
		guard let device = AudioObjectID.system.defaultInputDevice else {
			throw Failure("Can't retrieve default input device.") }
		
		return device
	}
	
	private
	func parseList() throws {
		let defaultInput = AudioObjectID.system.defaultInputDevice
		let defaultOutput = AudioObjectID.system.defaultOutputDevice
		
		func report(of aDevice: AudioDeviceID) -> Report {
			let isDefault = aDevice == defaultInput || aDevice == defaultOutput
			
			return aDevice.report + .entry("Default", isDefault.toYesNo())
		}
		
		let reports = AudioObjectID.system.devices
			.map { report(of: $0) }
			.map { String(describing: $0) }
			.joined(separator: "\n\n")
		
		print(reports)
	}
	
	private
	func parseShow() throws {
		try print(defaultOutputDevice().report)
	}
	
	private
	func parseDevice() throws -> AudioDeviceID {
		let idOrUID = try parseString(onErrorThrow: "Device is missing.")
		
		// The format of "ID" and "UID" is not clearly distinguishable. `Id`s
		// always start with "0x" and `UID`s can also start with "0x" -- but
		// don't have to
		guard let id = getDeviceID(from: idOrUID) ?? getDeviceUID(from: idOrUID)
			else { throw Failure("Can't find device.") }
		
		return id
	}
	
	///
	/// Returns an audio device id if the string addresses a "live" device with
	/// the specified ID.
	///
	private
	func getDeviceID(from idOrUID: String) -> AudioDeviceID? {
		guard
			(idOrUID.hasPrefix("0x") || idOrUID.hasPrefix("0X")),
			let id: AudioDeviceID = idOrUID.dropFirst(2).toInt(radix: 16)?.toExactUInt32(),
			id.isAlive
			else { return nil }
		
		return id
	}
	
	///
	/// Returns an audio device id if the string addresses a "live" device with
	/// the specified UID.
	///
	private
	func getDeviceUID(from: String) -> AudioDeviceID? {
		guard
			let id = AudioObjectID.system.lookup(by: from),
			id.isAlive
			else { return nil }
		
		return id
	}
	
	private
	func parseLevelOption() throws -> Option {
		switch try parseString(onErrorThrow: "Subcommand is missing.") {
		case "mute":	return .mute
		case "unmute":	return .unmute
		case "set":		return try parseLevelSet()
		case "change":	return try parseLevelChange()
		default:		throw Failure("Unknown subcommand.")
		}
	}
	
	private
	func parseLevelSet() throws -> Option {
		let level = try parseInt(onErrorThrow: "Can't set volume. Missing level.")
			.checked(in: 0...100, onErrorThrow: "Can't use the level. Range is 0...100.")
		
		return .set(level: level)
	}
	
	private
	func parseLevelChange() throws -> Option {
		let delta = try parseInt(onErrorThrow: "Can't change volume. Missing delta.")
			.checked(in: -100...100, onErrorThrow: "Can't use the delta. Range is -100...100.")
		
		return .change(delta: delta)
	}
	
}
