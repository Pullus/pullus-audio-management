import CoreAudio

extension AudioObjectID {
	
	var report: Report {
		return nameReport +
			idReport +
			uidReport +
			classReport +
			inputReport +
		outputReport
	}
	
	private
	var idReport: Report {
		return.entry("ID", String(format: "0x%04x", self))
	}
	private
	var nameReport: Report {
		return name.map { .entry("Name", $0) } ?? .empty
	}
	
	private
	var uidReport: Report {
		return uid.map { .entry("UID", $0) } ?? .empty
	}
	
	private
	var classReport: Report {
		return audioClass?.audioClassName.map { .entry("Class", $0) } ?? .empty
	}
	
	// MARK: - Input Report
	
	private
	var inputReport: Report {
		return isInputDevice
			? .entry("Input", "yes") + inputMuteReport + inputVolumeReport
			: .empty
	}
	
	private
	var inputMuteReport: Report {
		return hasInputVolume
			? .entry("Mute", String(describing: inputMute, default: "?"))
			: .empty
	}
	
	private
	var inputVolumeReport: Report {
		return hasInputVolume
			? .entry("Volume", String(describing: inputVolume, default: "?"))
			: .empty
	}
	
	// MARK: - Ouput Report
	
	private
	var outputReport: Report {
		return isOutputDevice
			? .entry("Output", "yes") + outputMuteReport + outputVolumeReport
			: .empty
	}
	
	private
	var outputMuteReport: Report {
		return hasOutputVolume
			? .entry("Mute", String(describing: outputMute, default: "?"))
			: .empty
	}
	
	private
	var outputVolumeReport: Report {
		return hasOutputVolume
			? .entry("Volume", String(describing: outputVolume, default: "?"))
			: .empty
	}
	
}
