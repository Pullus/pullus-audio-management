import Foundation

indirect
enum Report : CustomStringConvertible {
	
	case empty
	
	case entry(String, String)
	
	case list([Report])
	
	// MARK: - CustomStringConvertible
	
	var description: String {
		switch self {
		case .empty:
			return ""
			
		case let .entry(name, value):
			let indent = String(repeating: " ", count: 10 - name.count)
			return String(format: "%@%@: %@", indent, name, value)
			
		case let .list(reports):
			return reports.map { String(describing: $0)}.joined(separator: "\n")
		}
	}
	
}

func + (lhs: Report, rhs: Report) -> Report {
	switch (lhs, rhs) {
	case (.empty, _):	return rhs
	case (_, .empty):	return lhs
		
	case (.entry, .entry):	return .list([lhs, rhs])

	case (.entry, .list(let reports)):	return .list([lhs] + reports)
	case (.list(let reports), .entry):	return .list(reports + [rhs])
		
	case (.list(let lReports), .list(let rReports)):	return .list(lReports + rReports)

	default: return .empty
	}
	
}
