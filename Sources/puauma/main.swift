import Foundation

let ok = PullusAudioManager().run(arguments: CommandLine.arguments)

exit(ok ? 0 : 1)
