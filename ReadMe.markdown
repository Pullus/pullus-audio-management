Pullus AudioManagement 25.11.2022
=================================

A Swift framework and command line tool to change the input level and volume of MacOS audio devices.


Command line tool
-----------------

`puauma` (**P**ull**u**s **Au**dio **Ma**nager) is a command line tool to inspect and change the connected audio devices.

### Example: Devices

Show all audio devices:

```
puauma list
```

### Example: Default volume

Mute the default audio output device:

```
puauma volume mute
```

### Example: Specific device

Set the volume a specfic audio output device with the ID `0x003a` to 50%:

```
puauma output 0x003a set 50
```

### Example: Input level

Mute a specific audio input device with the UID `AppleHDAEngineInput:1B,0,1,2:5`:

```
puauma input AppleHDAEngineInput:1B,0,1,2:5 mute
```


### Help

```
NAME
    puauma - Pullus Audio Manager

SYNOPSIS
    Manage connected audio devices.

    help
        Shows this text.

    list
        Shows all audio device.

    show
        Shows the volume and the mute/unmute state of the default audio device.

    volume set <level>
        Sets the volume of the default audio device to a level (0 to 100).

    volume change <delta>
        Changes the volume of the default audio device by a delta (-100 to 100).

    volume mute
        Mutes the default audio device.

    volume unmute
        Unmutes the default audio device.

    sensitivity set <level>
        Sets the sensitivity of the default audio input device to a level (0 to 100).

    sensitivity change <delta>
        Changes the sensitivity of the default audio device by a delta (-100 to 100).

    sensitivity mute
        Mutes the default audio device.

    sensitivity unmute
        Unmutes the default audio device.

    output <device> set <level>
        Sets the volume of the specifided audio output device to a level (0 to 100).

    output <device> change <delta>
        Changes the volume of the specifided audio output device by a delta (-100 to 100).

    output <device> mute
        Mutes the specifided audio output device.

    output <device> unmute
        Unmutes the specifided audio output device.

    input <device> set <level>
        Sets the volume of the specifided audio input device to a level (0 to 100).

    input <device> change <delta>
        Changes the volume of the specifided audio input device by a delta (-100 to 100).

    input <device> mute
        Mutes the specifided audio input device.

    input <device> unmute
        Unmutes the specifided audio input device.


    *) A <device> is an ID or an UID. Both are shown with `list`.

EXAMPLES
    puauma volume mute

    Mutes the default audio device.

AUTHOR
    Frank Schuster
```


Framework
---------

### API Design

The framework uses a very lean approach to facilitate the use of the `CoreAudio` framework. 

The implementation avoids additional classes or structures. In particular, no wrappers are used. Instead, the existing types are extended.

This has some advantages but also disadvantages. For example, the types `AudioObjectID` and `AudioDeviceID` from `CoreAuio` are extended. These are aliases of `UInt32` and thus **every** `UInt32` is extended – which can be confusing.


Requirements
------------

Developed and tested with MacOS 10.12 and Xcode 9.2.

This software was intentionally developed for macOS 10.12 to be able to continue using older devices.
